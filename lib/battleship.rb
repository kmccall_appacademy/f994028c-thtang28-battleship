require_relative "board"
require_relative "player"

class BattleshipGame
  attr_reader :board, :player

  def initialize(player = HumanPlayer.new("Terrence"), board = Board.random)
    @board = board
    @player = player
  end

  def attack(position)
    board[position] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    pos = player.get_play
    attack(pos)
  end

  def play
    until game_over?
      play_turn
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  BattleshipGame.new.play
end
