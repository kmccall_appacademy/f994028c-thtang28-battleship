class Board
  attr_accessor :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    arr = Array.new(10) { Array.new(10)}
  end

  def count
    grid.flatten.select { |el| el == :s }.length
  end

  def empty?(position = nil)
    if position
      self[position].nil?
    else
      count == 0
    end
  end

  def [](position)
    row, col = position
    grid[row][col]
  end

  def []=(position, value)
    row, col = position
    grid[row][col] = value
  end

  def full?
    grid.flatten.none?(&:nil?)
  end

  def place_random_ship
    raise "Full" if full?
    random_position = [rand(grid.length), rand(grid.length)]

    until empty?(random_position)
      random_position = [rand(grid.length), rand(grid.length)]
    end

    self[random_position] = :s
  end

  def self.random
    count = 1
    until count == 10
      place_random_ship
      count += 1
    end
  end

  def won?
    grid.flatten.all?(&:nil?)
  end

  def display

  end

  DISPLAYHASH = {

  }
end
