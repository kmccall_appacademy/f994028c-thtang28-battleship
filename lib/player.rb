class HumanPlayer
  attr_accessor :name

  def initialize(name)
    @name = name
  end

  def get_play
    puts "Where do you want to attack?"
    gets.chomp.split(",").map { |el| Integer(el) }
  end
end
